import face_recognition
from flask_cors import CORS

database = []

global_user = "None"



def getFacesCount(image):
    return len(face_recognition.face_locations(image))


def addScan(face_encodings, user):
    for face_encoding in face_encodings:
        database.append({"user": user, "face_encoding": face_encoding})


def matchFace(face_encoding):
    results = {}
    for record in database:
        comparing_result = face_recognition.compare_faces(
            record['face_encoding'], face_encoding)
        if comparing_result[0]:
            if record['user'] in results.keys():
                results[record['user']] += 1
            else:
                results[record['user']] = 1
 
    max_comparisons = 0
    max_user = None
    for user in results:
        if max_comparisons < results[user]:
            max_comparisons = results[user]
            max_user = user
 
    return max_user
 

import cv2
import logging
import threading
import time
l = threading.Lock()

def thread_function():
    global global_user
    cap = cv2.VideoCapture(0)
    cap_for_user_adding = cv2.VideoCapture('cutted_pawel.mp4')
    cap_for_user_adding2 = cv2.VideoCapture('cutted_mateusz.mp4')

    if (cap.isOpened() == False):
        print("Error opening video stream or file")

    count = 0

    while True:
        count += 1
        ret, frame = cap.read()
        if ret:
            cv2.imshow('Frame', frame)
            if count % 15 == 0:
                user = None
                if getFacesCount(frame) > 0:
                    user = matchFace(face_recognition.face_encodings(frame))
                    if user:
                        pass #set in api.
                    else:
                        pass #set in api not recognized
                print(user)
                
                with open('somefile.txt', 'w') as the_file:
                    the_file.write(str(user))
    
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
    
            if count == 15:
                count2 = 0
                while cap_for_user_adding.isOpened():
                    count2 += 6
                    ret_2, frame_2 = cap_for_user_adding.read()
                    if ret_2 and count2 % 1 == 0:
                        addScan(face_recognition.face_encodings(frame_2), 'Paweł Nowak')
                    if not ret_2:
                        break
    
            if count == 20:
                count2 = 0
                while cap_for_user_adding2.isOpened():
                    count2 += 6
                    ret_2, frame_2 = cap_for_user_adding2.read()
                    if ret_2 and count2 % 1 == 0:
                        addScan(face_recognition.face_encodings(frame_2), 'Mateusz Nowak')
                    if not ret_2:
                        break
    
        else:
            break
    
    cap.release()
    cv2.destroyAllWindows()

from flask import (
    Flask,
    render_template
)


# Create the application instance
app = Flask(__name__, template_folder="templates")
cors = CORS(app, resources={r"/": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'

# Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    This function just responds to the browser ULR
    localhost:5000/

    :return:        the rendered template 'home.html'
    """
    global global_user
    print("@ESTPAAM")
    #user = "None"

    with open('somefile.txt', 'r') as the_file:
        user = the_file.read()

    print("User in flask = ", user)

    if user == 'None':
        return "None"
    else:
        return user 


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    print("Main    : before creating thread")
    x = threading.Thread(target=thread_function)
    print("Main    : before running thread")
    x.start()
    print("Main    : wait for the thread to finish")
    print("Main    : all done")

    app.run(debug=True)
    x.join()